package com.bitcoiner.app;

import android.content.Context;
import android.util.Log;

import com.bitcoiner.app.models.History;
import com.bitcoiner.app.models.Rate;
import com.bitcoiner.app.request.HttpRequest;
import com.bitcoiner.app.request.OnCallbackHttpRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class BitcoinProvider {


    public static final String PRICE_API       = "http://rtler.com/currency/index.php?tsyms=BTC,EUR";
    public static final String HISTORY_MIN_API = "https://min-api.cryptocompare.com/data/histominute?fsym=BTC&tsym=USD&limit=60&aggregate=3&e=CCCAGG";
    public static final String HISTORY_DAY_API = "https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&limit=60&aggregate=3&e=CCCAGG";


    private Context context;

    public BitcoinProvider(Context context) {
        this.context = context;
    }

    public void getPrices(final OnPriceListener listener) {
        new HttpRequest(context)
                .setRequestMethod(HttpRequest.GET)
                .setURL(PRICE_API)
                .get(new OnCallbackHttpRequestListener() {
                    @Override
                    public void onSuccessResponse(JSONObject json, String content) {
                        try {

                            //USD is top level currency!
                            double btc = json.getDouble("BTC");
                            double irr = json.getDouble("IRR");


                            ;
                            ;
                            ;


                            listener.onReceivePrice(btc, irr);
                            listener.onReceiveCurrencies(exchangeRate(btc), exchangeRate(irr));
                            listener.onReceiveRates(
                                    new Rate(json.getJSONObject("rates_30_min")),
                                    new Rate(json.getJSONObject("rates_hour")),
                                    new Rate(json.getJSONObject("rates_day"))
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailureResponse(int httpCode, String dataError) {
                        Log.i("TAG", "onFailureResponse: ");
                    }

                    @Override
                    public void onFailureConnection() {
                        listener.onFailureConnection();

                    }
                });

    }

    public void getHistoryDay(final OnHistoryListener listener) {
        new HttpRequest(context)
                .setRequestMethod(HttpRequest.GET)
                .setURL(HISTORY_MIN_API)
                .get(new OnCallbackHttpRequestListener() {
                    @Override
                    public void onSuccessResponse(JSONObject jsonObject, String content) {
                        try {
                            List<History> list = new ArrayList<>();
                            JSONArray array = jsonObject.getJSONArray("Data");
                            for (int i = array.length() - 1; i > 0; i--) {
                                JSONObject object = array.getJSONObject(i);
                                History history = new History(
                                        object.getLong("time"),
                                        object.getLong("high"),
                                        object.getLong("low")
                                );

                                list.add(history);
                            }

                            listener.onReceiveHistories(list);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailureResponse(int httpCode, String dataError) {

                    }

                    @Override
                    public void onFailureConnection() {

                    }
                });

    }


    public double exchangeRate(double target) {
        return (1 / target);
    }

    public interface OnPriceListener extends BaseBitcoinProviderListener {
        void onReceivePrice(double btc, double irr);

        void onReceiveCurrencies(double btcToUsd, double btcToIrr);


        void onReceiveRates(Rate halfHour, Rate hour, Rate day);
    }

    public interface OnHistoryListener extends BaseBitcoinProviderListener {
        void onReceiveHistories(List<History> histories);
    }

    public interface BaseBitcoinProviderListener {
        void onFailureConnection();
    }

}
