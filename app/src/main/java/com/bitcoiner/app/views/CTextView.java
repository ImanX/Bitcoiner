package com.bitcoiner.app.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class CTextView extends AppCompatTextView {

    public CTextView(Context context) {
        super(context);
    }

    public CTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/sansLight.ttf"));
        super.onFinishInflate();
    }


    public void setTextCurrentFormat(String currency) {
        try {
            this.setText(String.format("%1$,.0f", Double.parseDouble(currency)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
