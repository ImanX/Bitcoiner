package com.bitcoiner.app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bitcoiner.app.models.History;
import com.bitcoiner.app.views.CTextView;

import java.util.List;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private List<History> list;

    public Adapter(List<History> list){
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history,null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
         History history = this.list.get(position);
         holder.txtTime.setText(history.getTime());
         holder.txtHigh.setTextCurrentFormat(history.getHighValue() +"");
         holder.txtLow.setTextCurrentFormat(history.getLowValue() +"");
    }


    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        public CTextView txtTime;
        public CTextView txtHigh;
        public CTextView txtLow;


        public MyViewHolder(View itemView) {
            super(itemView);
            txtTime = itemView.findViewById(R.id.txt_time);
            txtHigh = itemView.findViewById(R.id.txt_high);
            txtLow = itemView.findViewById(R.id.txt_low);

        }
    }
}
