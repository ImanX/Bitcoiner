package com.bitcoiner.app.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class Rate {
    private double irr;
    private double btc;


    public Rate(JSONObject object){
        try {
            this.irr = object.getDouble("IRR");
            this.btc = object.getDouble("BTC");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Rate(double irr, double btc) {
        this.irr = irr;
        this.btc = btc;
    }

    public double getBtc() {
        return btc;
    }

    public double getIrr() {
        return irr;
    }
}
