package com.bitcoiner.app.models;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class History {
    private long time;
    private long highValue;
    private long lowValue;

    public History(long time, long highValue, long lowValue) {
        this.time = time;
        this.highValue = highValue;
        this.lowValue = lowValue;
    }

    public String getTime() {
         long sec = ((System.currentTimeMillis() /1000) - time) / 60;

        if (sec <  60){
            return "چند لحظه پیش";
        }


        return "چند دقیقه پیش";


    }

    public long getHighValue() {
        return highValue;
    }



    public long getLowValue() {
        return lowValue;
    }


}

