package com.bitcoiner.app;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by ImanX.
 * Bitconer | Copyrights 2017 ZarinPal Crop.
 */

public class NotifyService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.i("TAG", "onMessageReceived: " +  remoteMessage);
    }
}
