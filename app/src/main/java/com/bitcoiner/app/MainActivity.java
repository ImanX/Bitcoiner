package com.bitcoiner.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.bitcoiner.app.models.History;
import com.bitcoiner.app.models.Rate;
import com.bitcoiner.app.views.CTextView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;


public class MainActivity extends AppCompatActivity {


    private CTextView txtBitcoin;
    private CTextView txtDollar;
    private CTextView txtUsd2irr;


    private CardView cardFirst;
    private CardView cardSecond;
    private CardView cardList;
    private RecyclerView recyclerView;

    private void setupFCM(){
        FirebaseMessaging.getInstance().subscribeToTopic("btc");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setupFCM();


        this.txtBitcoin = (CTextView) findViewById(R.id.txt_usd_2_btc);
        this.txtDollar = (CTextView) findViewById(R.id.txt_btc_2_irr);
        this.txtUsd2irr = (CTextView) findViewById(R.id.txt_usd_2_irr);
        this.cardFirst = (CardView) findViewById(R.id.card_first);
        this.cardSecond = (CardView) findViewById(R.id.card_second);
        this.cardList = (CardView) findViewById(R.id.card_list);
        this.recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        new BitcoinProvider(this).getPrices(new BitcoinProvider.OnPriceListener() {

            @Override
            public void onReceivePrice(double btc, double irr) {

                Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dock_bottom_enter);
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dock_bottom_enter);
                //animation1.setDuration(1000);
                //animation1.setStartOffset(2000);
                //Animation animation2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dock_bottom_enter);
                animation.setStartTime(System.currentTimeMillis() + 5000);
                cardFirst.startAnimation(animation1);
                cardSecond.startAnimation(animation1);
                cardList.startAnimation(animation);
                animation1.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        cardFirst.setVisibility(View.VISIBLE);
                        cardSecond.setVisibility(View.VISIBLE);
                        cardList.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });




                txtUsd2irr.setTextCurrentFormat(String.format("%.0f", irr / 10));

            }

            @Override
            public void onReceiveRates(Rate halfHour, Rate hour, Rate day) {

            }

            @Override
            public void onReceiveCurrencies(double btcToUsd, double btcToIrr) {
                txtBitcoin.setTextCurrentFormat(String.format("%.0f", btcToUsd));
                txtDollar.setText(String.format("%6f", btcToIrr));

                //Log.i("TAG", "onReceiveCurrencies: " +String.format("%1$,.0f" , "10000000000000"));
            }

            @Override
            public void onFailureConnection() {

            }
        });




        new BitcoinProvider(this).getHistoryDay(new BitcoinProvider.OnHistoryListener() {
            @Override
            public void onReceiveHistories(List<History> histories) {
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL,false));
                recyclerView.setAdapter(new Adapter(histories));
            }

            @Override
            public void onFailureConnection() {

            }
        });





    }
}
