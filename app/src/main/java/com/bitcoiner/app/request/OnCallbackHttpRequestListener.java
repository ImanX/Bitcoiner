package com.bitcoiner.app.request;

import org.json.JSONObject;

/**
 * Android android-pg-sdk Project at ZarinPal
 * Created by ImanX on 5/7/17.
 * Copyright Alireza Tarazani All Rights Reserved.
 */

public interface OnCallbackHttpRequestListener {
    void onSuccessResponse(JSONObject jsonObject, String content);

    void onFailureResponse(int httpCode, String dataError);

    void onFailureConnection();
}
